package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface

data class SideBarMainTitleProperties(var titleColor: Int?,var iconColor: Int?, var titleTypeface: Typeface?)