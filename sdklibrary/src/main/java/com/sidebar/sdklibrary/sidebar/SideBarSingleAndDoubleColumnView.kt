package com.sidebar.sdklibrary.sidebar

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.response.CollectionSection
import com.sidebar.sdklibrary.sidebar.AllViewProperties.MainAllProperties
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarSingleAndDoubleColumnProperties
import com.sidebar.sdklibrary.sidebar.accordionContentView.SideBarAccordionContentHtml
import com.sidebar.sdklibrary.sidebar.accordionContentView.SideBarAccordionContentSummary
import com.sidebar.sdklibrary.sidebar.singleDoubleColumnContentView.SideBarSingleDoubleColumnContentImage
import java.util.ArrayList

@SuppressLint("ViewConstructor")
class SideBarSingleAndDoubleColumnView(context: Context, var activity: AppCompatActivity?, var mainSideBarMenu:MainSideBarMenu): RelativeLayout(context) {


    private var title: TextView? = null
    private var divider: View? = null
    private var singleDoubleColumnContentView: LinearLayout? = null
    private var mainLayout : LinearLayout? = null
    var userAgreementTrueArray: ArrayList<String>? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_single_and_doublecolumn,this)

        title = view.findViewById(R.id.title)
        divider = view.findViewById(R.id.divider)
        singleDoubleColumnContentView = view.findViewById(R.id.singleDoubleColumnContentView)
        mainLayout = view.findViewById(R.id.mainLayout)
        userAgreementTrueArray = ArrayList()

    }

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun lineVisibilityGone(){
        divider?.visibility = View.GONE
    }

    fun setTitle(title: String?){

        this.title?.text = title

    }

    fun setProperties(properties: SideBarSingleAndDoubleColumnProperties?){

        if (properties?.titleColor != null){

            this.title?.setTextColor(properties.titleColor!!)

        }

        if (properties?.titleTypeface != null){

            this.title?.typeface = properties.titleTypeface

        }

        if (properties?.lineViewColor != null){

            divider?.setBackgroundColor(properties.lineViewColor!!)

        }

    }

    fun userAgreementArray(userAgreementTrueArray: ArrayList<String>?){

        this.userAgreementTrueArray = userAgreementTrueArray

    }

    fun setDataSideBarMenu(data: List<CollectionSection>?, mainAllProperties: MainAllProperties?){

        singleDoubleColumnContentView?.removeAllViews()

        for (i in 0..data?.size!!-1){


            when(data.get(i).lsection?.typeStr){

                "image" -> {

                    val view = SideBarSingleDoubleColumnContentImage(this.context)

                    view.setData(data.get(i).lsection?.previewImage,data.get(i).lsection?.title)
                    view.setProperties(mainAllProperties?.sideBarSingleDoubleColumnContentImageProperties)

                    if (data.get(i).lsection?.viewTypeStr.equals("link")){

                        view.setOnClickListener {

                            if (data.get(i).lsection?.link != null){

                                val triggerString = data.get(i).lsection?.link?.split(":")
                                if (triggerString?.get(1).equals("null")){

                                }else{
                                    mainSideBarMenu.sideBarToastPopUpp(triggerString?.get(1),data.get(i).lsection?.androidLink)
                                }

                            }

                        }

                    }

                    singleDoubleColumnContentView?.addView(view)

                }

                "video" -> {

                    val view = SideBarAccordionContentHtml(this.context,mainSideBarMenu)

                    if (i == 0){
                        view.lineVisibilityGone()
                    }

                    //view.sendSideMenu(mainSideBarMenu)
                    //view.setDataWebView("https://s3linspire.metodbox.com/1_rmvpR5oiAoXGqrRT-c4e48180711f2c425f3bf9aa35d12d98.mp4")
                    view.setDataPreviewImage(data.get(i).lsection?.previewImage,data.get(i).lsection?.mediaURL,null,data.get(i).lsection?.title,mainAllProperties)
                    view.imageView?.visibility = View.VISIBLE

                    singleDoubleColumnContentView?.addView(view)

                }

                "rich" -> {

                    val view = SideBarAccordionContentHtml(this.context,mainSideBarMenu)

                    if (i == 0){
                        view.lineVisibilityGone()
                    }

                    if (data.get(i).lsection?.viewTypeStr.equals("popup")){
                        view.setDataPreviewImage(data.get(i).lsection?.previewImage,null,data.get(i).lsection?.html,data.get(i).lsection?.title,mainAllProperties)
                    }else{
                        view.webView?.visibility = View.VISIBLE
                        view.setHtml(data.get(i).lsection?.html)
                    }

                    singleDoubleColumnContentView?.addView(view)

                }

                "summary" -> {

                    val view = SideBarAccordionContentSummary(this.context)

                    if (i == 0){
                        view.lineVisibilityGone()
                    }

                    view.setProperties(mainAllProperties?.sideBarAccordionContentSummaryProperties)
                    view.lineVisibilityGone()
                    view.setTitleAndSubTitleData(data.get(i).lsection?.title,data.get(i).lsection?.summary)

                    singleDoubleColumnContentView?.addView(view)

                }

            }

        }

    }

}