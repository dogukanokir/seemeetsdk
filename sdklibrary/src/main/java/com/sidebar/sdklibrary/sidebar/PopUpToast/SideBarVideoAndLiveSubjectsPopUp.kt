package com.sidebar.sdklibrary.sidebar.PopUpToast

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.core.content.ContextCompat
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarPopUpProperties
import com.sidebar.sdklibrary.sidebar.MainSideBarMenu
import top.defaults.drawabletoolbox.DrawableBuilder


class SideBarVideoAndLiveSubjectsPopUp(context: Context, var mainSideBarMenu: MainSideBarMenu?): Dialog(context, R.style.Theme_AppCompat_Light_Dialog) {

    private var alertView: RelativeLayout? = null
    private var backgroundLayout: RelativeLayout? = null
    private var headerLayout: RelativeLayout? = null
    private var headerTextView: TextView? = null
    private var bodyLayout: LinearLayout? = null
    private var textView: TextView? = null
    private var webViewVideo: WebView? = null
    private var webViewLayout: RelativeLayout? = null
    private var webViewLayoutHtml: RelativeLayout? = null
    private var webViewHtml: WebView? = null


    private var tabLayout: LinearLayout? = null
    private var tabLayoutVideo: LinearLayout? = null
    private var titleText: TextView? = null
    private var titleVideoText: TextView? = null


    init {
        //this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.grey_color))
        this.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        val v = View.inflate(context, R.layout.layout_sidebar_video_livesubjects_popup,null)

        alertView = v.findViewById(R.id.alertView)
        backgroundLayout = v.findViewById(R.id.backgroundLayout)
        headerLayout = v.findViewById(R.id.headerLayout)
        headerTextView = v.findViewById(R.id.headerTextView)
        bodyLayout = v.findViewById(R.id.bodyLayout)
        textView = v.findViewById(R.id.textView)
        webViewVideo = v.findViewById(R.id.webViewVideo)
        webViewLayout = v.findViewById(R.id.webViewLayout)
        webViewLayoutHtml = v.findViewById(R.id.webViewLayoutHtml)
        webViewHtml = v.findViewById(R.id.webViewHtml)


        tabLayout = v.findViewById(R.id.tabLayout)
        tabLayoutVideo = v.findViewById(R.id.tabLayoutVideo)
        titleText = v.findViewById(R.id.titleText)
        titleVideoText = v.findViewById(R.id.titleVideoText)


        alertView?.setOnClickListener {

            mainSideBarMenu?.ad?.dismiss()

        }

        //alertView?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.grey_color)).build()


        setContentView(v)

    }

    fun setPropertiesVideoHtmlPopUp(sideBarPopUpProperties: SideBarPopUpProperties?){

        if (sideBarPopUpProperties?.popUpMainBackgroundColor != null){

            alertView?.setBackgroundColor(sideBarPopUpProperties.popUpMainBackgroundColor!!)

        }

        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(sideBarPopUpProperties?.popupLayoutLeftMargin!!,sideBarPopUpProperties.popupLayoutTopMargin,sideBarPopUpProperties.popupLayoutRightMargin,sideBarPopUpProperties.popupLayoutBottomMargin)

        //webViewLayout?.layoutParams = params
        webViewLayoutHtml?.layoutParams = params

        if (sideBarPopUpProperties.headerTitleBackgroundColor != null){

            if (sideBarPopUpProperties.radius != null){
                titleText?.background = DrawableBuilder().solidColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()
                titleVideoText?.background = DrawableBuilder().solidColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()
            }else{
                titleText?.setBackgroundColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor))
                titleVideoText?.setBackgroundColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor))
            }

        }

        if (sideBarPopUpProperties.headerTitleColor != null){
            titleText?.setTextColor(Color.parseColor(sideBarPopUpProperties.headerTitleColor))
            titleVideoText?.setTextColor(Color.parseColor(sideBarPopUpProperties.headerTitleColor))
        }

        if (sideBarPopUpProperties.headerTitleGravity != null){
            titleText?.gravity = sideBarPopUpProperties.headerTitleGravity!! or Gravity.CENTER
            titleVideoText?.gravity = sideBarPopUpProperties.headerTitleGravity!! or Gravity.CENTER
        }

        if (sideBarPopUpProperties.headerTitleIconLeft != null){
            titleText?.setCompoundDrawablesWithIntrinsicBounds(sideBarPopUpProperties.headerTitleIconLeft!!,0,0,0)
            titleVideoText?.setCompoundDrawablesWithIntrinsicBounds(sideBarPopUpProperties.headerTitleIconLeft!!,0,0,0)
        }

        if (sideBarPopUpProperties.headerTitleIconRight != null){
            titleText?.setCompoundDrawablesWithIntrinsicBounds(0,0,sideBarPopUpProperties.headerTitleIconRight!!,0)
            titleVideoText?.setCompoundDrawablesWithIntrinsicBounds(0,0,sideBarPopUpProperties.headerTitleIconRight!!,0)
        }

        if (sideBarPopUpProperties.radius != null){

            //bodyLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(0,0,sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!).build()
            tabLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.transparent)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()
            tabLayoutVideo?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.transparent)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()

        }

        if (sideBarPopUpProperties.backgroundColor != null){
            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, sideBarPopUpProperties.backgroundColor!!))
        }else{
            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.grey_color))
        }

    }


    fun setVariables(url: String?,title: String?){

        webViewHtml?.onPause()
        webViewVideo?.onResume()


        //tabLayoutVideo?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(40,40,0,0).build()
        //titleVideoText?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black_color)).cornerRadii(40,40,0,0).build()
        webViewLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(40,40,40,40).build()

        titleVideoText?.setTextColor(ContextCompat.getColor(context,R.color.white_color))

        titleVideoText?.text = title?.toUpperCase()


        webViewLayout?.visibility = View.VISIBLE

        webViewVideo?.settings?.javaScriptEnabled = true
        webViewVideo?.settings?.loadWithOverviewMode = true
        //webViewVideo?.settings?.useWideViewPort = true
        //webViewVideo?.settings?.setSupportZoom(false)
        webViewVideo?.settings?.builtInZoomControls = false
        webViewVideo?.settings?.allowContentAccess = true
        webViewVideo?.settings?.domStorageEnabled =true

        webViewVideo?.isScrollContainer = true

        /*if (isTablet()){
            var param = webViewVideo?.layoutParams
            param?.height = 800

            webViewVideo?.layoutParams = param

        }

         */


        webViewVideo?.settings?.pluginState = WebSettings.PluginState.ON
        //settings?.setAppCacheEnabled(false)
        //webView?.clearCache(true)
        webViewVideo?.webChromeClient = WebChromeClient()
        //webViewVideo?.webChromeClient = MyChrome()
        //webViewVideo?.settings?.mediaPlaybackRequiresUserGesture = true


        this.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        backgroundLayout?.visibility = View.GONE
        webViewLayoutHtml?.visibility = View.GONE
        //webViewLayout?.visibility = View.VISIBLE

        //webViewVideo?.invalidate()
        webViewVideo?.loadUrl(url)

    }


    fun setHtmlVariables(html: String?,title: String?){

        //tabLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(40,40,0,0).build()
        //titleText?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black_color)).cornerRadii(40,40,0,0).build()
        webViewLayoutHtml?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(40,40,40,40).build()

        titleText?.setTextColor(ContextCompat.getColor(context,R.color.white_color))

        titleText?.text = title?.toUpperCase()


        webViewVideo?.onPause()
        webViewHtml?.onResume()

        webViewLayoutHtml?.visibility = View.VISIBLE

        webViewHtml?.settings?.javaScriptEnabled = true
        webViewHtml?.settings?.loadWithOverviewMode = true
        webViewHtml?.settings?.useWideViewPort = true
        //webViewVideo?.settings?.setSupportZoom(false)
        webViewHtml?.settings?.builtInZoomControls = false
        webViewHtml?.settings?.allowContentAccess = true
        webViewHtml?.settings?.domStorageEnabled =true
        webViewHtml?.isScrollContainer = true

        /*if (isTablet()){
            var param = webViewVideo?.layoutParams
            param?.height = 800

            webViewVideo?.layoutParams = param

        }
         */


        webViewHtml?.settings?.pluginState = WebSettings.PluginState.ON
        //settings?.setAppCacheEnabled(false)
        //webView?.clearCache(true)
        //webViewHtml?.webChromeClient = WebChromeClient()
        var changeFontHtml = changedHeaderHtml(html!!)
        webViewHtml?.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {

                val css = "@import url(\"https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap\");.l-insp-modal-open{overflow:hidden}.l-insp-modal{font-size:16px;position:fixed;top:0;right:0;bottom:0;left:0;outline:none;height:100%;width:100%;z-index:2147483647;overflow:auto;overflow-y:scroll;visibility:hidden;display:table}.l-insp-modal.l-insp-fade-in{visibility:visible}.l-insp-modal.l-insp-fade .modal-dialog{opacity:0;transform:scale(0.8) translateZ(0);transition:all 250ms}.l-insp-modal.l-insp-fade-in .modal-dialog{opacity:1;transform:scale(1) translateZ(0)}.l-insp-modal-dialog{z-index:2147483646;display:table-cell;vertical-align:middle}.l-insp-modal-content{position:relative;background-color:#ffffff;border:1px solid #999999;border:1px solid rgba(0,0,0,0.2);border-radius:3px;outline:none;box-shadow:0 1px 5px rgba(0,0,0,0.25);background-clip:border-box;width:50%;max-width:760px;min-width:320px;margin:auto;border-radius:10px;max-height:80vh;overflow:auto;color:#333;font-family:'Nunito', Verdana;font-size:15px;line-height:1.4}.l-insp-modal-content h1,.l-insp-modal-content .h1,.l-insp-modal-content h2,.l-insp-modal-content .h2,.l-insp-modal-content h3,.l-insp-modal-content .h3,.l-insp-modal-content h4,.l-insp-modal-content .h4,.l-insp-modal-content h5,.l-insp-modal-content .h5,.l-insp-modal-content h6,.l-insp-modal-content .h6{color:#333;margin:0 0 0.5em;font-family:'Nunito', Verdana;font-weight:700}.l-insp-modal-content h1,.l-insp-modal-content .h1,.l-insp-modal-content h2,.l-insp-modal-content .h2{line-height:1.2}.l-insp-modal-content h3,.l-insp-modal-content h4,.l-insp-modal-content h5,.l-insp-modal-content h6,.l-insp-modal-content .h3,.l-insp-modal-content .h4,.l-insp-modal-content .h5,.l-insp-modal-content .h6{line-height:1.3}.l-insp-modal-content h1,.l-insp-modal-content .h1{font-size:2.074em}.l-insp-modal-content h2,.l-insp-modal-content .h2{font-size:1.728em}.l-insp-modal-content h3,.l-insp-modal-content .h3{font-size:1.44em}.l-insp-modal-content h4,.l-insp-modal-content .h4{font-size:1.2em}.l-insp-modal-content h5,.l-insp-modal-content .h5{font-size:1em}.l-insp-modal-content h6,.l-insp-modal-content .h6{font-size:0.833em}.l-insp-modal-content p,.l-insp-modal-content cite,.l-insp-modal-content pre{margin-bottom:1.5em;color:#333;font-family:'Nunito', Verdana}.l-insp-modal-content .txt-link{color:#0071D4;text-decoration:underline}.l-insp-modal-content h3.modal-heading{width:calc(100% - 34px);margin:0;font-family:'Nunito', Arial;font-size:24px;font-weight:700;line-height:1.2}.l-insp-modal-content img{display:block;max-width:100%;height:auto}.l-insp-modal-content iframe,.l-insp-modal-content object,.l-insp-modal-content embed{max-width:100%;display:block}.l-insp-modal-content .l-insp-modal-resp-container{position:relative;overflow:hidden;padding-top:56.25%;margin-bottom:1.5em}.l-insp-modal-content .l-insp-modal-resp-container iframe,.l-insp-modal-content .l-insp-modal-resp-container .iframe{position:absolute;top:0;left:0;width:100%;height:100%;border:0}.l-insp-modal-content em,.l-insp-modal-content .italic{font-style:italic}.l-insp-modal-content ul{margin:0 0 30px 30px}.l-insp-modal-content ul li{list-style-type:disc;list-style-position:inside}.l-insp-modal-content ol{margin:0 0 30px 30px}.l-insp-modal-content ol li{list-style-type:decimal;list-style-position:inside}.l-insp-modal-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:2147483646;background-color:#111;visibility:hidden;transition:all .1s}.l-insp-modal-backdrop.fade{opacity:0}.l-insp-modal-backdrop.l-insp-fade-in{opacity:0.75;visibility:visible}.l-insp-modal-header{min-height:32px;padding:15px;border-bottom:1px solid #e5e5e5;position:sticky;top:0;left:0;background:#fff;z-index:1}.l-insp-modal-header .close{margin-top:-2px}.l-insp-modal-title{margin:0;line-height:1.428571429}.l-insp-modal-body{position:relative;padding:30px;line-height:1.4}.l-insp-modal-big-img{margin:0 auto 20px}.l-insp-modal-big-img img{margin:0 auto}.l-insp-close{position:absolute;top:12px;right:12px;width:32px;height:32px;font-size:26px;font-family:Verdana;font-weight:bold;line-height:1;color:#333;opacity:0.2;filter:alpha(opacity=20);text-decoration:none;display:flex;align-items:center;justify-content:center}.l-insp-close:hover,.l-insp-close:focus{color:#111;text-decoration:none;cursor:pointer;opacity:0.5;filter:alpha(opacity=50)}.l-insp-modal-footer{padding:15px;border-top:1px solid #e5e5e5;font-size:14px;position:sticky;bottom:0;left:0;background:#fff;z-index:1}.l-insp-modal-post{margin-bottom:48px}.l-insp-modal-post .date{font-size:12px;color:#999;font-style:italic}" +
                        "/*# sourceMappingURL=l-insp-modal.css.map */" +
                        ".lim-preview table td{border:1px solid #ccc;padding:.4rem}.lim-preview strong{font-weight:bold}.lim-preview em{font-style:italic}.lim-preview ul{list-style:disc;list-style-position:inside;margin:0 0 2em 2em}.lim-preview ol{list-style:decimal;list-style-position:inside;margin:0 0 2em 2em}" +
                        ".l-insp-modal-content img{margin-right: auto;margin-left: auto;}" +
                        ".lim-preview {padding: 8px;}" //your css as String
                val js = "var style = document.createElement('style'); style.innerHTML = '$css'; document.head.appendChild(style);"

                var baseTest = "javascript:AndroidFunction.resize(document.body.scrollHeight)"
                view.evaluateJavascript(js,null)
                super.onPageFinished(view, url)
            }
        }

        //webViewHtml?.loadUrl("https://mywepage.com") //webpage you want to load
        //webViewVideo?.webChromeClient = MyChrome()
        //webViewVideo?.settings?.mediaPlaybackRequiresUserGesture = true


        this.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        backgroundLayout?.visibility = View.GONE
        webViewLayout?.visibility = View.GONE

        //webViewVideo?.invalidate()

        val asd = html + "@import url(\"https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap\");.l-insp-modal-open{overflow:hidden}.l-insp-modal{font-size:16px;position:fixed;top:0;right:0;bottom:0;left:0;outline:none;height:100%;width:100%;z-index:2147483647;overflow:auto;overflow-y:scroll;visibility:hidden;display:table}.l-insp-modal.l-insp-fade-in{visibility:visible}.l-insp-modal.l-insp-fade .modal-dialog{opacity:0;transform:scale(0.8) translateZ(0);transition:all 250ms}.l-insp-modal.l-insp-fade-in .modal-dialog{opacity:1;transform:scale(1) translateZ(0)}.l-insp-modal-dialog{z-index:2147483646;display:table-cell;vertical-align:middle}.l-insp-modal-content{position:relative;background-color:#ffffff;border:1px solid #999999;border:1px solid rgba(0,0,0,0.2);border-radius:3px;outline:none;box-shadow:0 1px 5px rgba(0,0,0,0.25);background-clip:border-box;width:50%;max-width:760px;min-width:320px;margin:auto;border-radius:10px;max-height:80vh;overflow:auto;color:#333;font-family:'Nunito', Verdana;font-size:15px;line-height:1.4}.l-insp-modal-content h1,.l-insp-modal-content .h1,.l-insp-modal-content h2,.l-insp-modal-content .h2,.l-insp-modal-content h3,.l-insp-modal-content .h3,.l-insp-modal-content h4,.l-insp-modal-content .h4,.l-insp-modal-content h5,.l-insp-modal-content .h5,.l-insp-modal-content h6,.l-insp-modal-content .h6{color:#333;margin:0 0 0.5em;font-family:'Nunito', Verdana;font-weight:700}.l-insp-modal-content h1,.l-insp-modal-content .h1,.l-insp-modal-content h2,.l-insp-modal-content .h2{line-height:1.2}.l-insp-modal-content h3,.l-insp-modal-content h4,.l-insp-modal-content h5,.l-insp-modal-content h6,.l-insp-modal-content .h3,.l-insp-modal-content .h4,.l-insp-modal-content .h5,.l-insp-modal-content .h6{line-height:1.3}.l-insp-modal-content h1,.l-insp-modal-content .h1{font-size:2.074em}.l-insp-modal-content h2,.l-insp-modal-content .h2{font-size:1.728em}.l-insp-modal-content h3,.l-insp-modal-content .h3{font-size:1.44em}.l-insp-modal-content h4,.l-insp-modal-content .h4{font-size:1.2em}.l-insp-modal-content h5,.l-insp-modal-content .h5{font-size:1em}.l-insp-modal-content h6,.l-insp-modal-content .h6{font-size:0.833em}.l-insp-modal-content p,.l-insp-modal-content cite,.l-insp-modal-content pre{margin-bottom:1.5em;color:#333;font-family:'Nunito', Verdana}.l-insp-modal-content .txt-link{color:#0071D4;text-decoration:underline}.l-insp-modal-content h3.modal-heading{width:calc(100% - 34px);margin:0;font-family:'Nunito', Arial;font-size:24px;font-weight:700;line-height:1.2}.l-insp-modal-content img{display:block;max-width:100%;height:auto}.l-insp-modal-content iframe,.l-insp-modal-content object,.l-insp-modal-content embed{max-width:100%;display:block}.l-insp-modal-content .l-insp-modal-resp-container{position:relative;overflow:hidden;padding-top:56.25%;margin-bottom:1.5em}.l-insp-modal-content .l-insp-modal-resp-container iframe,.l-insp-modal-content .l-insp-modal-resp-container .iframe{position:absolute;top:0;left:0;width:100%;height:100%;border:0}.l-insp-modal-content em,.l-insp-modal-content .italic{font-style:italic}.l-insp-modal-content ul{margin:0 0 30px 30px}.l-insp-modal-content ul li{list-style-type:disc;list-style-position:inside}.l-insp-modal-content ol{margin:0 0 30px 30px}.l-insp-modal-content ol li{list-style-type:decimal;list-style-position:inside}.l-insp-modal-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:2147483646;background-color:#111;visibility:hidden;transition:all .1s}.l-insp-modal-backdrop.fade{opacity:0}.l-insp-modal-backdrop.l-insp-fade-in{opacity:0.75;visibility:visible}.l-insp-modal-header{min-height:32px;padding:15px;border-bottom:1px solid #e5e5e5;position:sticky;top:0;left:0;background:#fff;z-index:1}.l-insp-modal-header .close{margin-top:-2px}.l-insp-modal-title{margin:0;line-height:1.428571429}.l-insp-modal-body{position:relative;padding:30px;line-height:1.4}.l-insp-modal-big-img{margin:0 auto 20px}.l-insp-modal-big-img img{margin:0 auto}.l-insp-close{position:absolute;top:12px;right:12px;width:32px;height:32px;font-size:26px;font-family:Verdana;font-weight:bold;line-height:1;color:#333;opacity:0.2;filter:alpha(opacity=20);text-decoration:none;display:flex;align-items:center;justify-content:center}.l-insp-close:hover,.l-insp-close:focus{color:#111;text-decoration:none;cursor:pointer;opacity:0.5;filter:alpha(opacity=50)}.l-insp-modal-footer{padding:15px;border-top:1px solid #e5e5e5;font-size:14px;position:sticky;bottom:0;left:0;background:#fff;z-index:1}.l-insp-modal-post{margin-bottom:48px}.l-insp-modal-post .date{font-size:12px;color:#999;font-style:italic}\n" +
                "/*# sourceMappingURL=l-insp-modal.css.map */\n" +
                ".lim-preview table td{border:1px solid #ccc;padding:.4rem}.lim-preview strong{font-weight:bold}.lim-preview em{font-style:italic}.lim-preview ul{list-style:disc;list-style-position:inside;margin:0 0 2em 2em}.lim-preview ol{list-style:decimal;list-style-position:inside;margin:0 0 2em 2em}\n" +
                ".l-insp-modal-content img{margin-right: auto;margin-left: auto;}\n" +
                ".lim-preview {padding: 8px;}"
        webViewHtml?.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null)

    }
    public fun  changedHeaderHtml(htmlText:String):String {


        var head = "<head><meta name=\"viewport\" content=\"width=device-width,@import url(\"https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap\"), user-scalable=yes\" /></head>";

        var closedTag = "</body></html>";
        var changeFontHtml = head + htmlText + closedTag;
        return changeFontHtml;
    }
    /*class MyChrome : WebChromeClient() {

        private var mCustomView: View? = null
        private var mCustomViewCallback: WebChromeClient.CustomViewCallback? = null
        protected var mFullscreenContainer: FrameLayout? = null
        private var mOriginalOrientation: Int = 0
        private var mOriginalSystemUiVisibility: Int = 0


        override fun getDefaultVideoPoster(): Bitmap? {
            return if (mCustomView == null) {
                null
            } else BitmapFactory.decodeResource(rContext?.getResources(), 2130837573)
        }

        override fun onHideCustomView() {

            ((rContext as MainPageActivity).window.getDecorView() as FrameLayout).removeView(this.mCustomView)
            this.mCustomView = null
            (rContext as MainPageActivity).window.getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility)
            (rContext as MainPageActivity).requestedOrientation = this.mOriginalOrientation
            this.mCustomViewCallback!!.onCustomViewHidden()
            this.mCustomViewCallback = null
        }

        override fun onShowCustomView(paramView: View, paramCustomViewCallback: WebChromeClient.CustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView()
                return
            }
            this.mCustomView = paramView
            this.mOriginalSystemUiVisibility = (rContext as MainPageActivity).window.getDecorView().getSystemUiVisibility()
            this.mOriginalOrientation = (rContext as MainPageActivity).requestedOrientation
            this.mCustomViewCallback = paramCustomViewCallback
            ((rContext as MainPageActivity).window.getDecorView() as FrameLayout).addView(this.mCustomView , FrameLayout.LayoutParams(-1 ,-1))
            (rContext as MainPageActivity).window.getDecorView().setSystemUiVisibility(3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
        }

    }

     */


}