package com.sidebar.sdklibrary.sidebar.accordionContentView
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sidebar.sdklibrary.R

class SideBarAccordionContentImage(context: Context): RelativeLayout(context) {


    private var divider: View? = null
    private var title: TextView? = null
    private var subTitle: TextView? = null
    private var image: ImageView? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_accordion_content_image,this)

        divider = view.findViewById(R.id.divider)
        title = view.findViewById(R.id.title)
        subTitle = view.findViewById(R.id.subTitle)
        image = view.findViewById(R.id.image)

    }

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun lineVisibilityGone(){
        divider?.visibility = View.GONE
    }

    fun setTitleAndSubTitleData(title: String, titleColor: String?, subTitle: String, subTitleColor: String?){

        this.title?.text = title
        if (titleColor != null){
            this.title?.setTextColor(Color.parseColor(titleColor))
        }

        this.subTitle?.text = subTitle
        if (subTitleColor != null){
            this.subTitle?.setTextColor(Color.parseColor(subTitleColor))
        }

    }

    fun setImage(url: String){

        Glide.with(this).load(url).into(image!!)

    }



}