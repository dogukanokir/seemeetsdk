package com.sidebar.sdklibrary.network


interface NetworkResponseListener<Response> {
    fun onResponseReceived(response: Response)
    fun onEmptyResponse(response: Response?)
    fun onError(failMessage: Int, error: String)
}
